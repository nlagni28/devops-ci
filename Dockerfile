FROM openjdk:17-jdk-alpine
MAINTAINER angelo
COPY . ./
RUN chmod +xwr mvnw
RUN ./mvnw install
RUN cp target/demo-0.0.1-SNAPSHOT.jar app.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/app.jar"]
