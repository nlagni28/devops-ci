package com.devops.demo.controler;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingsController {

  @GetMapping("/welcome")
  private String greet() {
    return "Welcome sur notre page de demo";
  }
}
